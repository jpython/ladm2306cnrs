# NGINX reverse proxy to a "real" Apache Web Site

```
$ sudo systemctl stop apache2
$ sudo systemctl disable apache2
$ sudo apt install nginx openssl apache2-utils
```

Edit as root the configuration file `/etc/nginx/nginx.conf` so that it looks
like:
```
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
	worker_connections 768;
	# multi_accept on;
}

http {
  upstream realsrv {
          server real_apache_server_ip:80 weight=1;
  }
  server {
    listen 443 ssl;
    listen [::]:443 ssl;
    include ./self-signed.conf;
    include ./ssl-params.conf;

    server_name me.mydomain.org;

    location / {
       auth_basic            "Username and Password Required";
       auth_basic_user_file  /etc/nginx/.htpasswd;
       proxy_pass http://realsrv;
       proxy_set_header Host $host;
       proxy_set_header X-Real-IP $remote_addr;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
       proxy_set_header X-Forwarded-Proto $scheme;
     } 
  }

  server {
    listen 80;
    listen [::]:80;

    server_name me.mydomain.org;

    return 302 https://$server_name$request_uri;
  }
}
```

And create as root two auxiliary configuration files for SSL:

`/etc/nginx/self-signed.conf` :

~~~~
ssl_certificate /etc/nginx/ssl/nginx-selfsigned.crt;
ssl_certificate_key /etc/nginx/ssl/private/nginx-selfsigned.key;
~~~~

`/etc/nginx/ssl-params.conf` : 

~~~~
ssl_protocols TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_dhparam /etc/nginx/ssl/dhparam.pem;
ssl_ciphers ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384;
ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
ssl_session_timeout  10m;
ssl_session_cache shared:SSL:10m;
ssl_session_tickets off; # Requires nginx >= 1.5.9
ssl_stapling on; # Requires nginx >= 1.3.7
ssl_stapling_verify on; # Requires nginx => 1.3.7
add_header X-Frame-Options DENY;
add_header X-Content-Type-Options nosniff;
add_header X-XSS-Protection "1; mode=block";
~~~~

Create the certificate files:

```
admin@ip-10-0-0-42:~$ sudo mkdir -p /etc/nginx/ssl/private
admin@ip-10.0.0-42:~$ sudo chmod a=rwx,go= /etc/nginx/ssl/private
admin@ip-10-0-0-42:~$ cd /etc/nginx/ssl
admin@ip-10-0-0-42:~$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -keyout private/nginx-selfsigned.key \
    -out ./nginx-selfsigned.crt

...
Generating a RSA private key
......................+++++
..........................+++++
writing new private key to 'private/nginx-selfsigned.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:FR
State or Province Name (full name) [Some-State]:Bretagne
Locality Name (eg, city) []:Brest
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Flying Circus
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:me.mydomain.org
Email Address []:contact@mydomain.org

$ sudo openssl dhparam -out dhparam.pem 2048
```

Preparing the authentication user file for Nginx and set the
user's password, for instance for a login name _billg_:
```
admin@ip-10-0-0-42:~$ sudo htpasswd -c /etc/nginx/.htpasswd billg
```

Here is the complete NGINX configuration layout:
~~~~
$ tree /etc/nginx
.../nginx
└── etc
    ├── nginx
    │   ├── nginx.conf
    │   ├── self-signed.conf
    │   └── ssl-params.conf
    └── ssl
        ├── dhparam.pem
        ├── nginx-selfsigned.crt
        └── private
            └── nginx-selfsigned.key
~~~~ 

You can restart nginx then:
```
dmin@ip-10-0-0-42:~$ sudo systemctl restart nginx
``` 
You can test by going to url https://your_public_ip/ with a Web navigator.


