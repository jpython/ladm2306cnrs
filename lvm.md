une fois ajoutés trois disques :

~~~~Bash
$ lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda      8:0    0   10G  0 disk 
└─sda1   8:1    0   10G  0 part /usr/local
sdb      8:16   0   20G  0 disk 
├─sdb1   8:17   0  512M  0 part /boot/efi
├─sdb2   8:18   0 18,5G  0 part /
└─sdb3   8:19   0  976M  0 part [SWAP]
sdc      8:32   0   10G  0 disk 
sdd      8:48   0   10G  0 disk 
sde      8:64   0   10G  0 disk 
sr0     11:0    1 1024M  0 rom  
~~~~

Mes nouveaux disques sont sdc, sdd et sde !

**Soyez sûr de bien noter les noms des nouveaux disques !!!!!
À ce stade il n'y a aucun doute : ces trois nouveaux disques sont
les seuls qui n'ont pas de partitions**

## Créer une partition unique sur chaque disque de type Linux LVM :

~~~~Bash
$ sudo cfdisk /dev/sdc
$ sudo cfdisk /dev/sdd
$ sudo cfdisk /dev/sde
~~~~

Pour chaque disque on arrive à cet affichage (ne pas oublier d'écrire les modifications
avant de quitter cfdisk) :

~~~~
                                         Disque : /dev/sdc
                       Taille : 10 GiB, 10737418240 octets, 20971520 secteurs
                Étiquette : gpt, identifiant : 475399C5-3C8B-6640-91BC-09DC2141BA20

    Périphérique                Début             Fin        Secteurs        Taille Type
>>  /dev/sdc1                    2048        20971486        20969439           10G LVM Linux       







 ┌────────────────────────────────────────────────────────────────────────────────────────────────┐
 │       UUID de la partition : 70D27B8F-A750-3749-86D3-589075060754                              │
 │       Type de la partition : LVM Linux (E6D6D379-F507-44C2-A23C-238F2A3DF928)                  │
 │UUID du système de fichiers : 3LXoa7-J4WO-mStP-XElJ-2Bws-uneO-7f4xtO                            │
 │        Système de fichiers : LVM2_member                                                       │
 └────────────────────────────────────────────────────────────────────────────────────────────────┘
      [   Supprimer  ]  [Redimensionner]  [    Quitter   ]  [     Type     ]  [     Aide     ]
      [    Écrire    ]  [  Sauvegarder ]

          Écrire la table de partitions sur le disque (des données peuvent être détruites)
~~~~

On peut vérifier que tout est ok avec `fdisk` :

~~~~Bash
$ sudo fdisk -l /dev/sd[cde] | grep '^/dev'
/dev/sdc1     2048 20971486 20969439    10G LVM Linux
/dev/sdd1     2048 20971486 20969439    10G LVM Linux
/dev/sde1     2048 20971486 20969439    10G LVM Linux
~~~~

~~~~Bash
$ lsblk
...
sdc      8:32   0   10G  0 disk 
└─sdc1   8:33   0   10G  0 part 
sdd      8:48   0   10G  0 disk 
└─sdd1   8:49   0   10G  0 part 
sde      8:64   0   10G  0 disk 
└─sde1   8:65   0   10G  0 part 
~~~~

## Initialiser les volumes physiques

~~~~Bash
$ sudo apt install lvm2
$ sudo pvcreate /dev/sdc1
$ sudo pvcreate /dev/sdd1
$ sudo pvcreate /dev/sde1
$ sudo pvdisplay
$ sudo pvscan
~~~~

## Créer un groupe de volumes contenant ces trois volumes physiques

~~~~Bash
$ sudo vgcreate myvg /dev/sdc1 /dev/sdd1 /dev/sde1 # ou ... /dev/sd[cde]1
$ sudo vgscan
$ sudo vgdisplay
$ sudo pvscan
$ sudo pvdisplay
~~~~

## Création de deux volumes logiques nommées data et cnrs :

13G pour data et le reste pour cnrs :

~~~~Bash
$ sudo lvcreate --name data -L 13G myvg
  Logical volume "data" created.
$ sudo lvcreate --name cnrs -l 100%FREE myvg
  Logical volume "cnrs" created.
$ sudo lvscan
$ sudo lvdisplay
~~~~

Et aussi : vg/pv scan/display.

La sortie de `lsblk` est magnifique :

~~~~Bash
sdc             8:32   0   10G  0 disk 
└─sdc1          8:33   0   10G  0 part 
  └─myvg-data 254:0    0   13G  0 lvm  
sdd             8:48   0   10G  0 disk 
└─sdd1          8:49   0   10G  0 part 
  ├─myvg-data 254:0    0   13G  0 lvm  
  └─myvg-cnrs 254:1    0   17G  0 lvm  
sde             8:64   0   10G  0 disk 
└─sde1          8:65   0   10G  0 part 
  └─myvg-cnrs 254:1    0   17G  0 lvm  
~~~~

## Formatage et montage des deux volumes logique en xfs et ext4

~~~~Bash
$ sudo mkfs.xfs  /dev/myvg/data
$ sudo mkfs.ext4 /dev/myvg/cnrs
~~~~

~~~Bash
$ sudo mkdir /srv/data /srv/cnrs
$ sudo vi /etc/fstab
~~~~

Ajouter deux lignes :

~~~~
/dev/myvg/data  /srv/data  xfs  defaults 0 2
/dev/myvg/cnrs  /srv/cnrs  ext4 defaults 0 2
~~~~

Tester : 

~~~~Bash
$ sudo mount -a
$ lsblk
$ lsblk --fs
$ df -h /srv/cnrs /srv/data
Sys. de fichiers      Taille Utilisé Dispo Uti% Monté sur
/dev/mapper/myvg-cnrs    17G     24K   16G   1% /srv/cnrs
/dev/mapper/myvg-data    13G    126M   13G   1% /srv/data
~~~~

# Encore plus fort : ajouter un disque et augmenter la taille de nos volumes logiques

Arrêtez votre système Linux et ajoutez (c'est la dernière fois) encore un disque
de 10Gio.

Démarrez votre système et identifiez le nom du disque :

~~~~Bash
$ lsblk
...
sdf             8:80   0   10G  0 disk 
sr0            11:0    1 1024M  0 rom  
~~~~

C'est `/dev/sdf` (seul disque non partitionné)

Créez une partition unique de type Linux LVM avec `cfdisk`.

Il ne reste plus qu'à initialiser cette partition comme volume physique,
puis l'ajouter au groupe de volume myvg. Ensuite on peut étendre chaque
volume logique comme on veut, par exemple :

~~~~Bash
$ sudo pvcreate /dev/sdf1
$ sudo vgextend myvg /dev/sdf1
  Volume group "myvg" successfully extended
$ sudo vgdisplay
...
Free  PE / Size       2559 / <10,00 GiB
...
$ sudo lvextend -L +2G /dev/myvg/data
  Size of logical volume myvg/data changed from 13,00 GiB (3328 extents) to 15,00 GiB (3840 extents).
  Logical volume myvg/data successfully resized.
$ df -h /srv/data /srv/cnrs
Sys. de fichiers      Taille Utilisé Dispo Uti% Monté sur
/dev/mapper/myvg-data    13G    126M   13G   1% /srv/data
/dev/mapper/myvg-cnrs    17G     24K   16G   1% /srv/cnrs
$ sudo xfs_growfs /srv/data
$ sudo lvextend --resizefs -l +50%FREE /dev/myvg/cnrs
$ df -h /srv/data /srv/cnrs
~~~~

Note : avec l'option `--resizefs` la commande `lvextend` va automatiquement
exécuter la commande adapté au système de fichier (`xfs_growfs` ou `resize2fs`
pour ext2, ext3 ou ext4). 

J'ai volontairement laissé un peu d'espace libre dans le groupe de volume
ce qui permet, entre autre, de créer des _snapshots_ (instantanés) de nos
volumes (à des fins de sauvegarde par exemple).

~~~~Bash
$ sudo lvcreate --snapshot --name backup -l 100%FREE /dev/myvg/data
  Logical volume "backup" created.
$ sudo mount -o nouuid /dev/myvg/backup /mnt
$ df -h /mnt/
Sys. de fichiers        Taille Utilisé Dispo Uti% Monté sur
/dev/mapper/myvg-backup    19G    169M   19G   1% /mnt
$ sudo umount /mnt
$ sudo lvremove /dev/myvg/backup
Do you really want to remove active logical volume myvg/backup? [y/n]: y
  Logical volume "backup" successfully removed
~~~~

